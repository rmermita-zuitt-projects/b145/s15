// confirm message
// console.log() is used to output messages in the console of your browser.
console.log('Hello from JS');
// There are multiple ways to display messages in the console using different formats.

// Error message
console.error('Error Message');

// Warning Message
console.warn('Warning Message');

// syntax:
// function nameOfFunction() {

// }

function errorMessage() {
	console.error('Error!');
}

errorMessage(); //callout the function

function greetings() { //Declaration
	// procedures
	console.log('Salutations from JS!');
}

greetings();

//There are different methods in declaring a function in JS
// [Section 1] variables declared outside a function can be used INSIDE a function.

let givenName = 'John';
let familyName = 'Smith';

// 'let' create a function that will utilize the information outside its scope.
function fullName() {
	// combine the values of the information outside and display it inside the console.
	console.log(givenName + ' ' + familyName);
}

// Invoking/Calling out functions we do it by adding parenthesis after the function name.
fullName();

// [Section 2] 'Blocked Scope' variables, this means that variables can only be used within the scope of the function.
function computeTotal () {
	let numA = 20;
	let numB = 5;
	//add the values and display it inside the console.
	console.log(numA + numB);
}

computeTotal();

// [Section 3] Functions with parameters
// Parameter acts as a variable or a container/catchers that only exists inside a function. A parameter is used to store information that is provided to a function when it is called or invoked.

//create a function that emulate a pokemon battle.
function pokemon(pangalan) {
	//We're going to use the "parameter" declared on this function to be processed and displayed inside the console.
	console.log('I choose you: ' + pangalan);
}

pokemon('Pikachu');

//parameters vs arguments
//argument is the actual value that is provided inside a function for it to work properly. The term argument is used when functions are called/invoked as compared to parameteres when a function is simply declared.

//[Section 4] Functions with multiple parameters

//declare a function that will get the sum of multiple values.
function addNumbers(numA, numB, numC, numD, numE) {
	// display the sum inside the console
	console.log(numA + numB + numC + numD + numE);
}

//NaN means Not a Number
addNumbers(1,2,3,4,5);

//create a function that will generate a person's full name.
function createFullName(fName, mName, lName) {
	console.log(lName + ',' + fName + ' ' + mName);
}

createFullName('Juan', 'Gomez', 'Dela Cruz');
//upon using multiple arguments it will correspond to the number of 'parameters' declared in a function in succeeding order, unless reordered inside the function.

//[Section 5] using variables as arguments

//create a function that will display the stats of a pokemon in a battle.

let attackA = 'Tackle';
let attackB = 'Thunderbolt';
let attackC = 'Lightning';
let selectedPokemon = 'Ratata';

function pokemonStats(name, attack) {
	console.log(name + ' use ' + attack);
}

pokemonStats(selectedPokemon, attackA);

//Note: using variables as arguments will allow us to utilize code reusability. This will help us reduce the amount of code that we have to write.

//Use of the 'return' statement.
//The return statement is used to display the output of a function.
//Return statement allows the output of a function to be passed to the line/block of code that called the function.
//[Section 6] The return statement
//create a function that will return a string message.
function returnString() {
	return 'Hello Wanda Ganda'
	 2 + 1;
}

//repackage the result of this function inside a new variable.
let functionOutput = returnString();
console.log(functionOutput);

//create a simple function to demonstrate the use of behaviour of the return statement again.
function dialog (){
	console.log("It's Britney Bitch");
	console.log('Sometimes I run, sometimes I hide');
	return "I'm a slave for you!";
	console.log('Toxic!');
}
//Note: any block/line of code that will come after our 'return' statement is going to be ignored because it came after the end of the function execution.
//Main Purpose of the return statement is to identify which will be the final output of the function and at the same time determine the end of the function statement.
console.log(dialog());

//[Section 7] using functions as arguments.

//Function parameters can also accept other functions as their argument.
//Some complex functions use other functions as their arguments to perform more complicated result.

//Create a simple function to be used as an argument later.
function argumentSaFunction() {
	console.log('This function was passed as an argument');
}

function invokeFunction(argumentNaFunction, pangalawangFunction) {
	argumentNaFunction();
	pangalawangFunction(selectedPokemon, attackA);
	pangalawangFunction(selectedPokemon, attackB);
}

invokeFunction(argumentSaFunction, pokemonStats);

//If you want to see details about a function
console.log(invokeFunction); //to find information about a function inside the console.